package synchronization.raceCondition.bank;

import java.util.Collections;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class UnsyncBank {
    private final List<Account> accounts;

    private UnsyncBank(int numOfAccounts, double initialBalance) {
        Account account = new Account(initialBalance);
        this.accounts = Collections.nCopies(numOfAccounts, account);
    }

    public static UnsyncBank create(int numOfAccounts, double initialBalance) {
        return new UnsyncBank(numOfAccounts, initialBalance);
    }

    public void transfer(int from, int to, double amount) {
        Account sender = accounts.get(from);
        Account receiver = accounts.get(to);
        if (sender.getBalance() < amount) return;
        System.out.print(Thread.currentThread());
        sender.decreaseBalance(amount);
        System.out.printf("%10.2f from %s to %s", amount, sender, receiver);
        receiver.increaseBalance(amount);
        System.out.printf("Total Balance: %10.2f\n", getTotalBalance());

    }

    public double getTotalBalance() {
        return accounts.stream()
                .mapToDouble(Account::getBalance)
                .sum();
    }

    public int size() {
        return accounts.size();
    }
}

class Account {
    private double initialBalance;

    public Account(double initialBalance) {
        this.initialBalance = initialBalance;
    }


    public double getBalance() {
        return initialBalance;
    }

    public void increaseBalance(double amount) {
        initialBalance += amount;
    }

    public void decreaseBalance(double amount) {
        initialBalance -= amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "initialBalance=" + initialBalance +
                '}';
    }
}