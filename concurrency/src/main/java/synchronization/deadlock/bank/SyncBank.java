package synchronization.deadlock.bank;

import java.util.Collections;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class SyncBank {
    private final List<Account> accounts;

    public SyncBank(int numOfAccounts, double initialBalance) {
        Account account = Account.create(initialBalance);
        this.accounts = Collections.nCopies(numOfAccounts, account);
    }

    public static SyncBank create(int numOfAccounts, double initialBalance) {
        return new SyncBank(numOfAccounts, initialBalance);
    }

    public synchronized void transfer(int from, int to, double amount) throws InterruptedException {
        Account sender = accounts.get(from);
        Account receiver = accounts.get(to);
        while (sender.getBalance() < amount) {
            this.wait();
        }
        System.out.print(Thread.currentThread());
        sender.decreaseBalance(amount);
        System.out.printf("%10.2f from %s to %s", amount, sender, receiver);
        receiver.increaseBalance(amount);
        System.out.printf("Total Balance: %10.2f\n", getTotalBalance());
        this.notifyAll();
    }

    public synchronized double getTotalBalance() {
        return accounts.stream()
                .mapToDouble(Account::getBalance)
                .sum();
    }

    public int size() {
        return accounts.size();
    }
}

class Account {
    private double initialBalance;

    private Account(double initialBalance) {
        this.initialBalance = initialBalance;
    }

    public static Account create(double initialBalance) {
        return new Account(initialBalance);
    }

    public double getBalance() {
        return initialBalance;
    }

    public void increaseBalance(double amount) {
        initialBalance += amount;
    }

    public void decreaseBalance(double amount) {
        initialBalance -= amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "initialBalance=" + initialBalance +
                '}';
    }
}
