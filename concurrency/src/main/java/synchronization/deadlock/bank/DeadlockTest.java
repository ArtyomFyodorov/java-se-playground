package synchronization.deadlock.bank;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Artyom Fyodorov
 */
public class DeadlockTest {
    private static final int N_ACCOUNTS = 10;
    private static final double INITIAL_BALANCE = 1_000;
    //  max amount exceeds initial balance, this leads to deadlock
    private static final double MAX_AMOUNT = 10 + INITIAL_BALANCE;
    //    private static final double MAX_AMOUNT = INITIAL_BALANCE;
    private static final long DELAY = 10;

    public static void main(String... args) {
        SyncBank bank = SyncBank.create(N_ACCOUNTS, INITIAL_BALANCE);

        List<Callable<Void>> works = IntStream.range(0, N_ACCOUNTS)
                .mapToObj(fromAccount -> (Callable<Void>) () -> {
                    while (true) {
                        int toAccount = (int) (bank.size() * Math.random());
                        double amount = MAX_AMOUNT * Math.random();
                        bank.transfer(fromAccount, toAccount, amount);
                        Thread.sleep(DELAY);
                    }
                }).collect(Collectors.toList());

        ExecutorService exService = Executors.newCachedThreadPool();
        try {
            exService.invokeAll(works);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            exService.shutdown();
        }
    }
}
