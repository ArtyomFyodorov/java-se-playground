package hash;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Artyom Fyodorov
 */
public class MD5Calculator {

    public static String compute(String path) throws IOException {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ignore) {/*NOP*/}

        assert md != null;
        byte[] input = Files.readAllBytes(Paths.get(path));
        BigInteger bigInteger = new BigInteger(1, md.digest(input));

        return bigInteger.toString(16);
    }
}
