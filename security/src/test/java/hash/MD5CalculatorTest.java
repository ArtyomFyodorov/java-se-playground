package hash;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Artyom Fyodorov
 */
class MD5CalculatorTest {

    @Test
    void check() throws Exception {
        final String pathToFile = "src/main/java/hash/MD5Calculator.java";
        final String exMd5Hash = MD5Calculator.compute(pathToFile);

        System.out.println(exMd5Hash);
        assertThat(exMd5Hash.length(), is(32));
        assertEquals(exMd5Hash, MD5Calculator.compute(pathToFile));
    }
}