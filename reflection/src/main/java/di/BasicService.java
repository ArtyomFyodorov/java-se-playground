package di;

/**
 * @author Artyom Fyodorov
 */
public class BasicService implements Service {
    @Override
    public String toString() {
        return "BasicService";
    }
}
