package di;

/**
 * @author Artyom Fyodorov
 */
public interface Service {
    // Service-specific methods go here
}
