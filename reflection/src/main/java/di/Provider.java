package di;

/**
 * @author Artyom Fyodorov
 */
public class Provider {

    private @PleaseInject("basic") Service service;

    public Service getService() {
        return service;
    }

    @Override
    public String toString() {
        return "Provider{" +
                "service=" + service +
                '}';
    }
}
