package di;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

/**
 * @author Artyom Fyodorov
 */
public class DependencyInjectionTest {
    public static void main(String... args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, IOException {
        String className = "di.Provider";
        Class<?> clazz = Class.forName(className);
        Object object = clazz.getDeclaredConstructor().newInstance();  // provider
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            PleaseInject annotation = field.getAnnotation(PleaseInject.class);
            if (Objects.nonNull(annotation)) {
                className = getClassName(annotation.value());
                clazz = Class.forName(className);
                Object instance = clazz.getDeclaredConstructor().newInstance();
                field.set(object, instance);
            }
        }
        System.out.println(object);
    }

    private static String getClassName(String name) throws IOException {
        Properties props = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("reflection/src/main/resources/services.properties"))) {
            props.load(in);
        }

        return props.getProperty(name);
    }
}
