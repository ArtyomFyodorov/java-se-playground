package threadsAndLocks.diningPhilosophersProblem;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Artyom Fyodorov
 */
public class Chopstick {
    private final Lock lock;

    public Chopstick() {
        this.lock = new ReentrantLock();
    }

    public boolean pickUp() {
        return lock.tryLock();
    }

    public void pickDown() {
        lock.unlock();
    }
}
