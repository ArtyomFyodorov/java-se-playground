package threadsAndLocks.diningPhilosophersProblem;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author Artyom Fyodorov
 */
public class Start {
    private static final int SIZE = 3;

    public static void main(String... args) {
        List<Chopstick> chopsticks = IntStream.range(0, SIZE + 1)
                .mapToObj(i -> new Chopstick())
                .collect(Collectors.toList());

        List<Philosopher> philosophers = IntStream.range(0, SIZE)
                .mapToObj(i -> {
                    Chopstick left = chopsticks.get(leftOf(i));
                    Chopstick right = chopsticks.get(rightOf(i));
                    return new Philosopher(i, left, right);
                }).collect(Collectors.toList());

        invokeAll(philosophers);

    }

    public static int leftOf(int i) {
        return i;
    }

    public static int rightOf(int i) {
        return (i + 1) % SIZE;
    }

    private static void invokeAll(List<Philosopher> philosophers) {
        ExecutorService executorService = Executors.newFixedThreadPool(philosophers.size());
        try {
            executorService.invokeAll(philosophers);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            executorService.shutdown();
        }
    }
}
