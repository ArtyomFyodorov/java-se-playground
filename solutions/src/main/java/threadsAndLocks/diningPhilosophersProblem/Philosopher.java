package threadsAndLocks.diningPhilosophersProblem;

import java.util.concurrent.Callable;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

/**
 * @author Artyom Fyodorov
 */
public class Philosopher implements Callable<Void> {
    private final Chopstick left;
    private final Chopstick right;
    private final long index;

    public Philosopher(int i, Chopstick left, Chopstick right) {
        this.left = left;
        this.right = right;
        this.index = i;
    }

    public void eat() {
        System.out.printf("Philosopher %d: start eating\n", index);
        if (pickUp()) {
            pause();  // chew
            putDown();
            System.out.printf("Philosopher %d: done eating\n", index);
        } else {
            System.out.printf("Philosopher %d: gave up on eating\n", index);
        }
    }

    public boolean pickUp() {
        pause();
        if (!left.pickUp()) return false;

        pause();
        if (!right.pickUp()) return false;

        pause();
        return true;
    }

    public void putDown() {
        right.pickDown();
        left.pickDown();
    }

    public void pause() {
        try {
            long pause = ThreadLocalRandom.current().nextLong(100, 500);
            Thread.sleep(pause);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Void call() throws Exception {
        int bites = 10;
        IntStream.range(0, bites).forEach(ignore -> eat());
        return null;
    }
}
