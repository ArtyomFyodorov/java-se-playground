package threadsAndLocks.fizzBuzz;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author Artyom Fyodorov
 */
public class FizzBuzzMulti {
    public static void main(String... args) {
        run(100);
    }

    private static void run(int n) {

        UnitOfWork fizzBuzz = new UnitOfWork(i -> i % 3 == 0 && i % 5 == 0, i -> i + ": FizzBuzz", n);
        UnitOfWork fizz = new UnitOfWork(i -> i % 3 == 0 && i % 5 != 0, i -> i + ": Fizz", n);
        UnitOfWork buzz = new UnitOfWork(i -> i % 3 != 0 && i % 5 == 0, i -> i + ": Buzz", n);
        UnitOfWork other = new UnitOfWork(i -> i % 3 != 0 && i % 5 != 0, i -> i + "", n);
        List<UnitOfWork> works = List.of(fizzBuzz, fizz, buzz, other);

        invokeAll(works);
    }

    private static void invokeAll(List<UnitOfWork> works) {
        ExecutorService executorService = Executors.newFixedThreadPool(works.size());
        try {
            executorService.invokeAll(works);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught");
        } finally {
            executorService.shutdown();
        }
    }

    private static final class UnitOfWork implements Callable<Void> {
        private final Predicate<Integer> validator;
        private final Function<Integer, String> printer;
        private final int max;
        private final AtomicInteger counter;

        private UnitOfWork(Predicate<Integer> validator, Function<Integer, String> printer, int max) {
            this.validator = validator;
            this.printer = printer;
            this.max = max;
            this.counter = new AtomicInteger(0);
        }

        @Override
        public Void call() throws Exception {
            while (true) {
                int current = counter.incrementAndGet();
                if (current > max) return null;
                if (validator.test(current)) System.out.println(printer.apply(current));
            }
        }
    }
}