package threadsAndLocks.fizzBuzz;

import java.util.stream.IntStream;

/**
 * @author Artyom Fyodorov
 */
public class FizzBuzzSingle {
    public static void main(String... args) {
        run(100);
    }

    public static void run(int n) {
        IntStream.range(1, n)
                .forEach(i -> {
                    if (i % 3 == 0 && i % 5 == 0) System.out.printf("%d: FizzBuzz\n", i);
                    else if (i % 3 == 0) System.out.printf("%d: Fizz\n", i);
                    else if (i % 5 == 0) System.out.printf("%d: Buzz\n", i);
                    else System.out.println(i);
                });
    }
}
