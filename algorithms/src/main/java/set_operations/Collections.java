package set_operations;

import java.util.Collection;
import java.util.HashSet;

/**
 * @author Artyom Fyodorov
 */
public class Collections {
    public static <T> Collection<T> union(Collection<T> a, Collection<T> b) {
        final Collection<T> result = new HashSet<>(a);
        result.addAll(b);

        return result;
    }

    public static <T> Collection<T> intersection(final Collection<T> a, final Collection<T> b) {
        final Collection<T> result = new HashSet<>(a);
        result.retainAll(b);

        return result;
    }

    public static <T> Collection<T> difference(final Collection<T> a, final Collection<T> b) {
        final Collection<T> result = new HashSet<>(a);
        result.removeAll(b);

        return result;
    }

    public static <T> Collection<T> symmetricDifference(final Collection<T> a, final Collection<T> b) {
        final Collection<T> union = union(a, b);
        final Collection<T> intersection = intersection(a, b);

        return difference(union, intersection);  // union.removeAll(intersection);;
    }

    public static <T> boolean isSubSet(final Collection<T> a, final Collection<T> b) {
        return b.containsAll(a);
    }

    public static <T> boolean isSuperSet(final Collection<T> a, final Collection<T> b) {
//        return isSubSet(b, a);
        return a.containsAll(b);
    }
}
