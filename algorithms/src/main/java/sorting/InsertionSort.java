package sorting;

import java.util.Comparator;

/**
 * @author Artyom Fyodorov
 */
public class InsertionSort {

    public static <T extends Comparable<? super T>> void sort(T[] a) {
        if (a.length == 0) return;

        for (int k = 0; k < a.length; k++) {
            T element = a[k];
            int index = k - 1;
            while (index >= 0 && a[index].compareTo(element) > 0) {
                a[index + 1] = a[index];
                index--;
            }
            a[index + 1] = element;
        }
    }

    public static <T> void sort(T[] a, Comparator<? super T> c) {
        if (a.length == 0) return;

        for (int k = 1; k < a.length; k++) {
            T element = a[k];
            int index = k - 1;
            while (index >= 0 && c.compare(a[index], element) > 0) {
                a[index + 1] = a[index];
                index--;
            }
            a[index + 1] = element;
        }
    }

    public static void sort(int[] a) {
        if (a.length == 0) return;

        for (int i = 1; i < a.length; i++) {
            int element = a[i];
            int index = i - 1;
            while (index >= 0 && a[index] > element) {
                a[index + 1] = a[index];
                index--;
            }
            a[index + 1] = element;
        }
    }
}
