package sorting;

import java.util.Comparator;

/**
 * @author Artyom Fyodorov
 */
public class SelectionSort {
    public static <T extends Comparable<? super T>> void sort(T[] a) {
        if (a.length == 0) return;

        for (int barrier = 0; barrier < a.length - 1; barrier++) {
            for (int index = barrier + 1; index < a.length; index++) {
                if (a[barrier].compareTo(a[index]) > 0) swap(a, barrier, index);
            }
        }
    }

    public static <T> void sort(T[] a, Comparator<? super T> c) {
        if (a.length == 0) return;

        for (int barrier = 0; barrier < a.length - 1; barrier++) {
            for (int index = barrier + 1; index < a.length; index++) {
                if (c.compare(a[barrier], a[index]) > 0) swap(a, barrier, index);
            }
        }
    }

    public static void sort(int[] a) {
        if (a.length == 0) return;

        for (int barrier = 0; barrier < a.length - 1; barrier++) {
            for (int index = barrier + 1; index < a.length; index++) {
                if (a[barrier] > a[index]) swap(a, barrier, index);
            }
        }
    }

    private static void swap(int[] x, int a, int b) {
        int temp = x[b];
        x[b] = x[a];
        x[a] = temp;
    }

    private static <T> void swap(T[] x, int a, int b) {
        T temp = x[b];
        x[b] = x[a];
        x[a] = temp;
    }
}
