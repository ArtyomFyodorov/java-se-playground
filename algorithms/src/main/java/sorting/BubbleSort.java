package sorting;

import java.util.Comparator;

/**
 * @author Artyom Fyodorov
 */
public class BubbleSort {

    public static <T extends Comparable<? super T>> void sort(T[] a) {
        if (a.length == 0) return;

        for (int barrier = a.length - 1; barrier >= 0; barrier--) {
            for (int index = 0; index < barrier; index++) {
                if (a[index].compareTo(a[index + 1]) > 0) swap(a, index);
            }
        }
    }

    public static <T> void sort(T[] a, Comparator<? super T> c) {
        if (a.length == 0) return;

        for (int barrier = a.length - 1; barrier >= 0; barrier--) {
            for (int index = 0; index < barrier; index++) {
                if (c.compare(a[index], a[index + 1]) > 0) swap(a, index);
            }

        }
    }

    public static void sort(int[] a) {
        if (a.length == 0) return;

        for (int barrier = a.length - 1; barrier >= 0; barrier--) {
            for (int index = 0; index < barrier; index++) {
                if (a[index] > a[index + 1]) swap(a, index);
            }
        }
    }

    private static void swap(int[] x, int i) {
        int temp = x[i];
        x[i] = x[i + 1];
        x[i + 1] = temp;
    }

    private static <T> void swap(T[] x, int i) {
        T tmp = x[i];
        x[i] = x[i + 1];
        x[i + 1] = tmp;
    }


}
