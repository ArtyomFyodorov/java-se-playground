package sorting;

import java.util.Comparator;

/**
 * @author Artyom Fyodorov
 */
public class QuickSort {

    public static void sort(int[] a) {
        if (a.length > 0) sort(a, 0, a.length - 1);
    }

    public static <T extends Comparable<? super T>> void sort(T[] a) {
        if (a.length > 0) sort(a, 0, a.length - 1);
    }

    public static <T> void sort(T[] a, Comparator<? super T> c) {
        if (a.length > 0) sort(a, 0, a.length - 1, c);
    }

    private static void sort(int[] a, int first, int last) {
        if (first < last) {
            int left = first, right = last, pivot = a[left + (right - left) / 2]; // to avoid overflow

            do {
                while (a[left] < pivot) left++;
                while (a[right] > pivot) right--;
                if (left <= right) {
                    swap(a, left, right);
                    left++;
                    right--;
                }
            } while (left <= right);

            sort(a, first, right);
            sort(a, left, last);
        }
    }

    private static <T extends Comparable<? super T>> void sort(T[] a, int first, int last) {
        if (first < last) {
            int left = first, right = last;
            T pivot = a[first + (last - first) / 2]; // to avoid overflow

            do {
                while (a[left].compareTo(pivot) < 0) left++;
                while (a[right].compareTo(pivot) > 0) right--;
                if (left <= right) {
                    swap(a, left, right);
                    left++;
                    right--;
                }
            } while (left <= right);

            sort(a, first, right);
            sort(a, left, last);
        }
    }

    private static <T> void sort(T[] a, int first, int last, Comparator<? super T> c) {
        if (first < last) {
            int left = first, right = last;
            T pivot = a[first + (last - first) / 2]; // to avoid overflow

            do {
                while (c.compare(a[left], pivot) < 0) left++;
                while (c.compare(a[right], pivot) > 0) right--;
                if (left <= right) {
                    swap(a, left, right);
                    left++;
                    right--;
                }
            } while (left <= right);

            sort(a, left, last, c);
            sort(a, first, right, c);
        }
    }

    private static void swap(int[] x, int i, int j) {
        int tmp = x[i];
        x[i] = x[j];
        x[j] = tmp;
    }

    private static <T> void swap(T[] x, int i, int j) {
        T tmp = x[i];
        x[i] = x[j];
        x[j] = tmp;
    }
}
