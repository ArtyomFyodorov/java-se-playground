package recursion;

/**
 * @author Artyom Fyodorov
 */
public class Fibonacci {

    public static int f(int number) {
        return number < 2 ? number :
                f(number - 2) + f(number - 1);
    }
}
