package reversion;

/**
 * @author Artyom Fyodorov
 */
public class ReverseArray {

    public static <T> void reverse(T[] a) {
        for (int i = 0; i < a.length >> 1; i++) {
            swap(a, i);
        }
    }

    public static void reverse(int[] a) {
        for (int i = 0; i < a.length >> 1; i++) {
            swap(a, i);
        }
    }

    private static void swap(int[] x, int i) {
        int temp = x[i];
        x[i] = x[x.length - i - 1];
        x[x.length - i - 1] = temp;
    }

    private static <T> void swap(T[] x, int i) {
        T temp = x[i];
        x[i] = x[x.length - i - 1];
        x[x.length - i - 1] = temp;
    }
}
