package recursion;

import org.junit.jupiter.api.Test;
import sorting.AbstractAlgoTest;

import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Artyom Fyodorov
 */
class FibonacciTest extends AbstractAlgoTest {
    @Test
    void findFibonacciValue() {
        IntStream.range(0, fibonacci[0].length)
                .forEach(i -> {
                    int expected = fibonacci[1][i];
                    int actual = Fibonacci.f(fibonacci[0][i]);
                    display(i, expected, actual);
                    assertThat(actual, is(expected));
                });
    }

}