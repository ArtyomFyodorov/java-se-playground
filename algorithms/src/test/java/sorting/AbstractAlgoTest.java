package sorting;

import org.junit.jupiter.api.BeforeEach;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * @author Artyom Fyodorov
 */
public class AbstractAlgoTest {

    protected int[][] integers;
    protected String[][] strings;
    protected int[][] fibonacci;

    @BeforeEach
    public void init() throws Exception {
        this.strings = new String[][]{
                {"banana"},
                {"Orange", "mango", "apple"},
                {"grapes", "Orange", "kiwi", "mango", "Guava"},
                {"apple", "Guava", "Orange", "mango", "grapes", "banana", "kiwi", "Grapefruit"},
        };

        this.integers = new int[][]{
                {},
                {1},
                {42, -42},
                {3, 2, 1},
                {-1, 1, 3, -2, 2},
                {8, 6, 3, 5, 4, 0, 1, 9, 2, 7},
        };

        this.fibonacci = new int[][]{
                {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
                {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765}
        };
    }

    protected void display(int[] original, int[] expected, int[] actual) {
        display(IntStream.of(original).boxed().toArray(),
                IntStream.of(expected).boxed().toArray(),
                IntStream.of(actual).boxed().toArray());
    }

    protected <T> void display(T[] original, T[] expected, T[] actual) {
        System.out.printf("original: %s\nexpected: %s\nactual\t: %s\n\n", Arrays.toString(original), Arrays.toString(expected), Arrays.toString(actual));
    }

    protected void display(int f, int expected, int actual) {
        System.out.printf("f(%d)\nexpected: %d\nactual\t: %d\n\n", f, expected, actual);
    }
}
