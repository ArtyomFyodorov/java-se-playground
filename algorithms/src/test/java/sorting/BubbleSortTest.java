package sorting;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artyom Fyodorov
 */
class BubbleSortTest extends AbstractAlgoTest {

    @Test
    void throwsNPEBecauseArgumentsAreNull() {
        assertThrows(NullPointerException.class, () -> BubbleSort.sort((int[]) null));
        assertThrows(NullPointerException.class, () -> BubbleSort.sort((String[]) null));
        assertThrows(NullPointerException.class, () -> BubbleSort.sort(null, Comparator.reverseOrder()));
    }

    @Test
    void primitiveTypeSortTest() {
        Arrays.stream(integers).forEach(actual -> {
            int[] original = actual.clone();
            int[] expected = actual.clone();
            Arrays.sort(expected);
            BubbleSort.sort(actual);
            display(original, expected, actual);
            assertTrue(Arrays.equals(expected, actual));
        });
    }

    @Test
    void refTypeSortTest() {
        Arrays.stream(strings).forEach(actual -> {
            String[] original = actual.clone();
            String[] expected = actual.clone();
            Arrays.sort(expected);
            BubbleSort.sort(actual);
            display(original, expected, actual);
            assertThat(actual, arrayContaining(expected));
        });
    }

    @Test
    void refTypeSortWithComparatorTest() {
        Arrays.stream(strings).forEach(actual -> {
            String[] original = actual.clone();
            String[] expected = actual.clone();
            Arrays.sort(expected, String::compareToIgnoreCase);
            BubbleSort.sort(actual, String::compareToIgnoreCase);
            display(original, expected, actual);
            assertThat(actual, arrayContaining(expected));
        });
    }

}