package set_operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artyom Fyodorov
 */
class CollectionsTest {

    private List<String> a;
    private List<String> b;

    @BeforeEach
    void init() {
        this.a = new ArrayList<>(Arrays.asList("1", "2", "3", "4", "5"));
        this.b = new ArrayList<>(Arrays.asList("3", "4", "5", "6", "7"));
    }

    @Test
    void unionTest() {
        final Collection<String> actualCollection = Collections.union(this.a, this.b);

        display("Union", actualCollection);

        assertThat(actualCollection, containsInAnyOrder("1", "2", "3", "4", "5", "6", "7"));
    }

    @Test
    void intersectionTest() {
        final Collection<String> actualCollection = Collections.intersection(this.a, this.b);

        display("Intersection", actualCollection);

        assertThat(actualCollection, containsInAnyOrder("3", "4", "5"));
    }

    @Test
    void differenceTest() {
        final Collection<String> actualCollection = Collections.difference(this.a, this.b);

        display("Difference (a - b)", actualCollection);

        assertThat(actualCollection, containsInAnyOrder("1", "2"));
    }

    @Test
    void symmetricDifferenceTest() {
        final Collection<String> actualCollection = Collections.symmetricDifference(this.a, this.b);

        display("Symmetric difference", actualCollection);

        assertThat(actualCollection, containsInAnyOrder("1", "2", "6", "7"));
    }

    @Test
    void a_is_subset_of_b() {
        this.a = this.a.subList(2, this.a.size());
        final boolean actualResult = Collections.isSubSet(this.a, this.b);

        display("Is 'a' a subset of 'b'", actualResult);

        assertTrue(actualResult);
    }

    @Test
    void a_is_not_superset_of_b() {
        this.a = this.a.subList(2, this.a.size());
        final boolean actualResult = Collections.isSuperSet(this.a, this.b);

        display("Is 'a' a superset of 'b'", actualResult);

        assertFalse(actualResult);
    }

    @Test
    void b_is_not_sub_set_of_a() {
        this.a = this.a.subList(2, this.a.size());
        final boolean actualResult = Collections.isSubSet(this.b, this.a);

        display("Is 'b' a subset of 'a'", actualResult);

        assertFalse(actualResult);
    }

    @Test
    void b_is_superset_of_a() {
        this.a = this.a.subList(2, this.a.size());
        final boolean actualResult = Collections.isSuperSet(this.b, this.a);

        display("Is 'b a superset of 'a'", actualResult);

        assertTrue(actualResult);
    }

    private void display(String message, Object o) {
        System.out.printf("Set a: %s%n", a);
        System.out.printf("Set b: %s%n", b);
        System.out.printf("%s: %s%n%n", message, o);
    }
}