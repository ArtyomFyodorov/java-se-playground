package reversion;

import org.junit.jupiter.api.Test;
import sorting.AbstractAlgoTest;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artyom Fyodorov
 */
public class ReverseArrayTest extends AbstractAlgoTest {

    @Test
    void primitiveTypeArrayReverseTest() {
        Arrays.stream(integers).forEach(actual -> {
            int[] original = actual.clone();
            int[] expected = IntStream.range(0, actual.length)
                    .map(i -> actual[actual.length - i - 1]).toArray();
            ReverseArray.reverse(actual);
            display(original, expected, actual);
            assertTrue(Arrays.equals(expected, actual));
        });
    }

    @Test
    void refTypeArrayReverseTest() {
        Arrays.stream(strings).forEach(actual -> {
            String[] original = actual.clone();
            String[] expected = IntStream.range(0, actual.length)
                    .mapToObj(i -> actual[actual.length - i - 1]).toArray(String[]::new);
            ReverseArray.reverse(actual);
            display(original, expected, actual);
            assertThat(actual, arrayContaining(expected));
        });
    }
}
