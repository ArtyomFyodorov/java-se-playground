package deeplyNestedCode;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class DeveloperRepository {
    private final List<Developer> team;

    public DeveloperRepository(List<Developer> team) {
        this.team = team;
    }

    //  The Smell
    /*public Developer findByLanguage(String name) {
        for (Developer dev : team) {
            for (String l : dev.getLanguages()) {
                if (name.equals(l)) {
                    return dev;
                }
            }
        }
        return null;
    }*/

    // Solution
    public Developer findByLanguage(String name) {
        return team.stream()
                .filter(dev -> dev.isKnownLanguage(name))
                .findFirst().orElse(null);
    }

    // Solution with the returned Optional
    /*public Optional<Developer> findByLanguage(String name) {
        return team.stream()
                .filter(dev -> dev.isKnownLanguage(name))
                .findFirst();
    }*/

}
