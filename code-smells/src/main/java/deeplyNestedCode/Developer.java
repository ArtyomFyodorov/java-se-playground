package deeplyNestedCode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Artyom Fyodorov
 */
public class Developer {
    private Set<String> languages;

    public Developer(String... languages) {
        this.languages = new HashSet<>(Arrays.asList(languages));
    }

    public void addLanguage(String name) {
        this.languages.add(name);
    }

    public Set<String> getLanguages() {
        return languages;
    }

    // This seems pretty wasteful - HashSet.contains(Object) method has constant time complexity O(1),
    // but using Streams allows to parallelise the operation.
    /*public boolean isKnownLanguage(String name) {
        return getLanguages().stream()
                .parallel()
                .anyMatch(name::equals);
    }*/

    public boolean isKnownLanguage(String name) {
        return getLanguages().contains(name);
    }
}
