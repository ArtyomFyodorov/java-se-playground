package connection;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;

/**
 * @author Artyom Fyodorov
 */
public class ConnectionPresenter {
    public static Connection getConnection() throws IOException, SQLException {
        Properties props = new Properties();
//        try (InputStream in = Files.newInputStream(Paths.get("src/main/resources/database.properties"))) {
        try (InputStream in = ConnectionPresenter.class.getClassLoader().getResourceAsStream("database.properties")) {
            props.load(in);
        }
        Optional<String> drivers = Optional.ofNullable(props.getProperty("jdbc.drivers"));
        drivers.ifPresent(d -> System.setProperty("jdbc.drivers", d));
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");

        return DriverManager.getConnection(url, username, password);

    }
}
