package connection;


import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Artyom Fyodorov
 */
class ConnectionPresenterIT {

    @Test
    void connectionTest() throws Exception {
        try (Connection connection = ConnectionPresenter.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet result = statement.executeQuery("SELECT 42 FROM SYS.SYSTABLES")) {
                assertTrue(result.next());
                assertThat(result.getInt(1), is(42));
            }
        }

    }
}